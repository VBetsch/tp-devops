# Variables
DOCKER_PORT := 80
LISTEN_PORT := 80
DOCKER_NAME := tp-devops
DOCKER_TAG := local
ANSIBLE_KEYPAIR := "ansible/keypair.pem"
AWS_HOSTNAME := ubuntu@ec2-16-16-24-4.eu-north-1.compute.amazonaws.com

all: remove build

# Dependencies
venv:
	@echo "Install venv"
	python -m venv
	source venv/bin/activate
	pip install -r requirements.txt
	@echo "Done"

# Commands
_build:
	docker build -t $(DOCKER_NAME):$(DOCKER_TAG) .

_create:
	docker create -i -t -p $(DOCKER_PORT):$(LISTEN_PORT) --name $(DOCKER_NAME) $(DOCKER_NAME):$(DOCKER_TAG)

start:
	docker start $(DOCKER_NAME)

exec:
	docker exec -it $(DOCKER_NAME) sh

stop:
	docker stop $(DOCKER_NAME)

_remove:
	docker remove $(DOCKER_NAME)

robot: venv
	robot robotframework/

playbook:
	cd ansible && ansible-playbook playbook.yaml

connect:
	ssh -i $(ANSIBLE_KEYPAIR) $(AWS_HOSTNAME)

# Abbreviations
build: _build _create start
create: _create start
remove: stop _remove

.PHONY: all build create start exec stop remove robot playbook connect
