FROM php:apache
RUN apt-get update && apt-get upgrade -y
ADD src /var/www/html
EXPOSE 80
CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
