<?php

class City
{
   public function getCityNameById($id)
   {
       if ($id == 1) {
           $city = "Bordeaux";
       } else if ($id == 2) {
           $city = "Toulouse";
       } else {
           $city = "Paris";
       }
       return $city;
   }
}
