<?php

require_once __DIR__ . '/log.php';
require_once __DIR__ . '/vendor/autoload.php'; // Autoload files using Composer autoload

use HelloWorld\SayHello;

log_file($log, __FILE__);

echo SayHello::world();
