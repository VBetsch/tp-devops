stages:
  - build
  - test
  - quality
  - ansible
  - package
  - security
  - deploy
  - acceptance

variables:
  # Quality
  SONAR_SOURCE_DIR: .
  SONAR_ANALYSIS_ARGS: >-
    -Dsonar.host.url=${SONAR_HOST_URL}
    -Dsonar.organization=vbetsch-gitlab
    -Dsonar.projectKey=vbetsch_tp-devops
    -Dsonar.projectName=tp-devops
    -Dsonar.projectBaseDir=${CI_PROJECT_DIR}
    -Dsonar.sources=${SONAR_SOURCE_DIR}
    -Dsonar.exclusions=${SONAR_EXCLUSIONS}
    -Dsonar.sourceEncoding=UTF-8

  # Deploy
  DEPLOY_ONLY: "false"
  IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG

  # Acceptance
  ROBOT_TESTS_DIR: "robotframework/"
  ROBOT_CLI: "run-tests-in-virtual-screen"

# ------------------------------ BUILD ------------------------------

composer:install:
  stage: build
  image: composer:lts
  script:
    - cd src && composer install
  artifacts:
    paths:
      - src/vendor/
  rules:
    - if: $DEPLOY_ONLY != "true"


# ------------------------------ TEST ------------------------------

phpunit-test:
  stage: test
  image: registry.gitlab.com/pipeline-components/phpunit:latest
  script:
    - php src/vendor/bin/phpunit tests
  rules:
    - if: $DEPLOY_ONLY != "true"

# ------------------------------ QUALITY ------------------------------

sonarcloud-check:
  stage: quality
  image:
    name: sonarsource/sonar-scanner-cli:latest
    entrypoint: [ "" ]
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - sonar-scanner $SONAR_ANALYSIS_ARGS
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"
    GIT_DEPTH: "0"
  rules:
    - if: $DEPLOY_ONLY != "true"

# ------------------------------ ANSIBLE ------------------------------
ansible-playbook:
  stage: ansible
  image: alpinelinux/ansible
  script:
    - cd ansible && ansible-playbook playbook.yaml
  rules:
    - if: $DEPLOY_ONLY != "true"

# ------------------------------ PACKAGE ------------------------------

build_image:
  stage: package
  image: docker:20.10.16
  variables:
    DOCKER_HOST: tcp://docker:2375
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: ""
  services:
    - docker:20.10.16-dind
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $IMAGE_TAG .
    - docker push $IMAGE_TAG
  rules:
    - if: $DEPLOY_ONLY != "true"
  tags:
    - aws

# ------------------------------ SECURITY ------------------------------

trivy_container_scanning:
  stage: security
  image:
    name: alpine:3.11
  variables:
    GIT_STRATEGY: fetch
  allow_failure: true
  before_script:
    - export TRIVY_VERSION=${TRIVY_VERSION:-v0.19.2}
    - apk add --no-cache curl docker-cli
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - curl -sfL https://raw.githubusercontent.com/aquasecurity/trivy/main/contrib/install.sh | sh -s -- -b /usr/local/bin ${TRIVY_VERSION}
    - curl -sSL -o /tmp/trivy-gitlab.tpl https://github.com/aquasecurity/trivy/raw/${TRIVY_VERSION}/contrib/gitlab.tpl
  script:
    - trivy --exit-code 0 --no-progress $IMAGE_TAG
  only:
    - merge_requests
    - main
    - master
    - develop
    - tags

# ------------------------------ DEPLOY ------------------------------

deploy_rds:
  stage: deploy
  image: registry.gitlab.com/gitlab-org/cloud-deploy/aws-base:latest
  variables:
    CLUSTER: tp-devops
    SERVICE: tp-devops-rds-service
  script:
    - aws ecs update-service --cluster $CLUSTER --service $SERVICE --desired-count 1 --force-new-deployment

# ------------------------------ ACCEPTANCE ------------------------------

robotframework:chrome:
  stage: acceptance
  image: registry.gitlab.com/docker42/rfind:master
  script:
    - $ROBOT_CLI $PABOT_OPT $ROBOT_OPT -v BROWSER:chrome $ROBOT_TESTS_DIR
  artifacts:
    paths:
      - output.xml
      - log.html
      - report.html
